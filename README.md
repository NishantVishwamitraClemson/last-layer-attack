# Last Layer Attack: Environment Setup and Lab Tasks #


### Environment Setup in Google Colab ###

* Open Google colab. You should be able to access it for free.
![google_colab](https://bitbucket.org/NishantVishwamitraClemson/last-layer-attack/downloads/google_colab.png)


* Upload and extract the github project on colab.
![upload_project](https://bitbucket.org/NishantVishwamitraClemson/last-layer-attack/downloads/upload_project.png)


* Check if you’re able to run the code on colab.
![successfully_run_code](https://bitbucket.org/NishantVishwamitraClemson/last-layer-attack/downloads/successfully_run_code.png)


* Replace model.h5 and run the code again to execute your attack.
![upload_modelh5](https://bitbucket.org/NishantVishwamitraClemson/last-layer-attack/downloads/upload_modelh5.png)

### Lab 2 Tasks ###

* Disclaimer: Please do Exercise 0-0 and Exercise 0-1 (corresponding solutions have been provided) from https://github.com/Kayzaks/HackingNeuralNetworks/tree/master/0_LastLayerAttack

* Then, please finish two additional exercises (Exercise 0-2 and Exercise 0-3) individually. 

* Instructions: For each question from the exercises, please give the answer with a supporting screenshot. You may not receive full points if the screenshot is missing.

* Exercise 0-2: Implement the same attack in a different way from the given solution, i.e., you can still change bias, but don’t increase the value to a very high value.  

* Exercise 0-3: Can you implement the same attack by changing the weights of the last layer, and not bias? If yes, explain and show how.




